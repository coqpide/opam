# OPAM repo #

This OPAM repository provides the pidetop package.


```
#!shell

opam repo add pide http://bitbucket.org/coqpide/opam.git
opam install pidetop
```

The pidetop package depends on Coq 8.5
